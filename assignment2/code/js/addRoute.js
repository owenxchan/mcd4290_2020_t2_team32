let fromLocation = []
let toLocation = []
let allLocation = []
let countryListKey = "COUNTRY_LIST_STORAGE"
mapboxgl.accessToken = 'pk.eyJ1IjoidG5ndTAxMTIiLCJhIjoiY2tjd3Jud21lMDU2NjJ0bG0xeWx0NDcwMSJ9.RFxXb37wxqSmRPqFD1ewvg';
let map = null
let onclicked = false

// Get Country from LocalStorage
function importCountry() {
    if (typeof (Storage) !== undefined) {

        // reading out list of ports from local storage. 
        let portAPIList = JSON.parse(localStorage.getItem(portAPIkey))
        let portUserList = JSON.parse(localStorage.getItem(portListKey))

        let countryList = []

        // loops puchs countries in to a array.                 
        if (portAPIList !== null) {
            for (let i = 0; i < portAPIList.ports.length; i++) {
                if (!(countryList.includes(portAPIList.ports[i].country))) {
                    countryList.push(portAPIList.ports[i].country)
                }
            }
        }

        if (portUserList !== null) {
            for (let i = 0; i < portUserList._portList.length; i++) {
                if (!(countryList.includes(portUserList._portList[i]._portCountry))) {
                    countryList.push(portUserList._portList[i]._portCountry)
                }
            }
        }

        // sorts the countryList array
        countryList.sort()
        countryList[countryList.length - 1] = "Other"
        localStorage.setItem(countryListKey, JSON.stringify(countryList))
    }
    else {
        alert("Local storage is unavailable")
    }

}

// this function creates frop down list for from country            
function fromCountry() {
    if (typeof (Storage) !== undefined) {

        let countryList = JSON.parse(localStorage.getItem(countryListKey))
        let selectList = document.getElementById("fromCountry")
        selectList.innerHTML = "<option></option>"

        for (let i = 0; i < countryList.length; i++) {
            let opt = document.createElement('option');
            opt.appendChild(document.createTextNode(countryList[i]));
            opt.value = countryList[i]
            selectList.appendChild(opt)
        }
    }
    else {
        alert("Local storage is unavailble")
    }
}

// function creates a drop down list of ports from the selected "from country" 
function fromCountryPort() {
    if (typeof (Storage) !== undefined) {

        let selectList = document.getElementById("fromCountry")
        let selectListValue = selectList.value

        if (selectListValue === "Other") {
            selectListValue = null
        }

        let portAPIList = JSON.parse(localStorage.getItem(portAPIkey))
        let portUserList = JSON.parse(localStorage.getItem(portListKey))

        let fromPortList = []

        //loops push ports' name into an array depending on the selected country 
        if (portAPIList !== null) {
            for (let i = 0; i < portAPIList.ports.length; i++) {
                if (portAPIList.ports[i].country === selectListValue) {
                    fromPortList.push(portAPIList.ports[i].name)
                }
            }
        }

        if (portUserList !== null) {
            for (let i = 0; i < portUserList._portList.length; i++) {
                if (portUserList._portList[i]._portCountry === selectListValue) {
                    fromPortList.push(portUserList._portList[i]._portName)
                }
            }
        }
        // sorts the port names
        fromPortList.sort()

        // creating a drop down list from port name
        let portSelectList = document.getElementById("fromPort")
        portSelectList.innerHTML = "<option></option>"

        for (let i = 0; i < fromPortList.length; i++) {
            let opt = document.createElement('option');
            opt.appendChild(document.createTextNode(fromPortList[i]));
            opt.value = fromPortList[i]
            portSelectList.appendChild(opt)
        }
    }
    else {
        alert("Local storage is unavailable")
    }
}

// creating a drop down list for destination countries
function toCountry() {
    if (typeof (Storage) !== undefined) {

        let countryList = JSON.parse(localStorage.getItem(countryListKey))
        let selectList = document.getElementById("toCountry")
        selectList.innerHTML = "<option></option>"

        for (let i = 0; i < countryList.length; i++) {
            let opt = document.createElement('option');
            opt.appendChild(document.createTextNode(countryList[i]));
            opt.value = countryList[i]
            selectList.appendChild(opt)
        }
    }
    else {
        alert("Local storage is unavailable")
    }

}

//function used to create a drop down list for destination ports
function toCountryPort() {
    if (typeof (Storage) !== undefined) {

        let selectList = document.getElementById("toCountry")
        let selectListValue = selectList.value

        if (selectListValue === "Other") {
            selectListValue = null
        }

        let portAPIList = JSON.parse(localStorage.getItem(portAPIkey))
        let portUserList = JSON.parse(localStorage.getItem(portListKey))

        let toPortList = []

        if (portAPIList !== null) {
            for (let i = 0; i < portAPIList.ports.length; i++) {
                if (portAPIList.ports[i].country === selectListValue) {
                    toPortList.push(portAPIList.ports[i].name)
                }
            }
        }

        if (portUserList !== null) {
            for (let i = 0; i < portUserList._portList.length; i++) {
                if (portUserList._portList[i]._portCountry === selectListValue) {
                    toPortList.push(portUserList._portList[i]._portName)
                }
            }
        }
        // sorts the destination port array
        toPortList.sort()

        let portSelectList = document.getElementById("toPort")
        portSelectList.innerHTML = "<option></option>"
        // creating a drop down list.
        for (let i = 0; i < toPortList.length; i++) {
            let opt = document.createElement('option');
            opt.appendChild(document.createTextNode(toPortList[i]));
            opt.value = toPortList[i]
            portSelectList.appendChild(opt)
        }
    }
    else {
        alert("Local storage is unavailable")
    }

}

// gets the Longitude and Latitude for 
function fromPort() {
    if (typeof (Storage) !== undefined) {

        let fromCountryRef = document.getElementById("fromCountry")
        let fromPortRef = document.getElementById("fromPort")
        let fromCountryValue = fromCountryRef.value

        if (fromCountryValue === "Other") {
            fromCountryValue = null
        }

        let fromPortValue = fromPortRef.value

        let portAPIList = JSON.parse(localStorage.getItem(portAPIkey))
        let portUserList = JSON.parse(localStorage.getItem(portListKey))

        fromLocation = []

        // loops are used to push the latitude and longitude for the selected ports 
        if (portAPIList !== null) {
            for (let i = 0; i < portAPIList.ports.length; i++) {
                if (portAPIList.ports[i].country === fromCountryValue && portAPIList.ports[i].name === fromPortValue) {
                    fromLocation.push(portAPIList.ports[i].lng)
                    fromLocation.push(portAPIList.ports[i].lat)
                }
            }
        }

        if (portUserList !== null) {
            for (let i = 0; i < portUserList._portList.length; i++) {
                if (portUserList._portList[i]._portCountry === fromCountryValue && portUserList._portList[i]._portName === fromPortValue) {
                    fromLocation.push(portUserList._portList[i]._portLongitude)
                    fromLocation.push(portUserList._portList[i]._portLatitude)
                }
            }
        }
    }
    else {
        alert("Local storage is unavailable")
    }
}

// function for getting the Longitude and Latitude fro destination port
function toPort() {
    if (typeof (Storage) !== undefined) {

        let toCountryRef = document.getElementById("toCountry")
        let toPortRef = document.getElementById("toPort")
        let toCountryValue = toCountryRef.value
        let toPortValue = toPortRef.value

        if (toCountryValue === "Other") {
            toCountryValue = null
        }

        let portAPIList = JSON.parse(localStorage.getItem(portAPIkey))
        let portUserList = JSON.parse(localStorage.getItem(portListKey))

        toLocation = []
        // pushes latitude and logitude values into an array 
        if (portAPIList !== null) {
            for (let i = 0; i < portAPIList.ports.length; i++) {
                if (portAPIList.ports[i].country === toCountryValue && portAPIList.ports[i].name === toPortValue) {
                    toLocation.push(portAPIList.ports[i].lng)
                    toLocation.push(portAPIList.ports[i].lat)
                }
            }
        }

        if (portUserList !== null) {
            for (let i = 0; i < portUserList._portList.length; i++) {
                if (portUserList._portList[i]._portCountry === toCountryValue && portUserList._portList[i]._portName === toPortValue) {
                    toLocation.push(portUserList._portList[i]._portLongitude)
                    toLocation.push(portUserList._portList[i]._portLatitude)
                }
            }
        }
    }
    else {
        alert("Local storage is unavailable")
    }
}

// Making the Map and display two destinations between 2 ports and line
function createRoute() {
    if (typeof (Storage) !== undefined) {

        let startDateRef = document.getElementById("startDate")
        let startDateValue = startDateRef.value
        onclicked = false

        if (fromLocation.length !== 0 && toLocation.length !== 0 && startDateValue != "") {

            allLocation = [fromLocation, toLocation]
            let middleLocation = []
            map = new mapboxgl.Map
                ({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    zoom: 1.5,
                });

            map.on('click', function (e) {
                let location = e.lngLat.wrap()
                let line = turf.lineString([allLocation[allLocation.length - 2], [location.lng, location.lat]]);
                let length = turf.length(line, { units: 'kilometres' });

                if (length >= 100) {
                    middleLocation.push([location.lng, location.lat])

                    let newMarker = new mapboxgl.Marker({ color: "orange" })
                        .setLngLat([location.lng, location.lat])
                        .addTo(map)

                    allLocation = [fromLocation]
                    for (let i = 0; i < middleLocation.length; i++) {
                        allLocation.push(middleLocation[i])
                    }
                    allLocation.push(toLocation)
                    showPath()
                    distanceBetweenPort()
                }
                else {
                    alert("The way point must be as least 100km away from the last waypoint!")
                }

            });

            //From Port

            let fromPopup = new mapboxgl.Popup({ offset: 15 })
                .setText("From Port")
                .addTo(map)

            let fromMarker = new mapboxgl.Marker({ color: "green" })
                .setLngLat(fromLocation)
                .addTo(map)
                .setPopup(fromPopup);

            // To Port

            let toPopup = new mapboxgl.Popup({ offset: 15 })
                .setText("To Port")
                .addTo(map)

            let toMarker = new mapboxgl.Marker({ color: "red" })
                .setLngLat(toLocation)
                .addTo(map)
                .setPopup(toPopup);

            map.panTo([(fromLocation[0] + toLocation[0]) / 2, (fromLocation[1] + toLocation[1]) / 2])

            routeInfo()

        }
        else {
            alert("Please enter all the reqired data!")
        }
    }
    else {
        alert("Local storage is unavailable")
    }

}

function showPath() {
    if (map.getSource('route') === undefined) {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': allLocation
                }
            }
        });
    }
    else {
        map.removeLayer('route').removeSource('route');
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': allLocation
                }
            }
        });
    }

    map.addLayer({
        'id': 'route',
        'type': 'line',
        'source': 'route',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'paint': {
            'line-color': '#888',
            'line-width': 8
        }
    });
}

// Display all information of the Route
function routeInfo() {

    let grid = document.getElementById("showShip")
    // deletes all the childs
    while (grid.hasChildNodes()) {
        grid.removeChild(grid.firstChild);
    }

    let selectShip = document.createElement("div")
    selectShip.className = "mdl-cell mdl-cell--3-col"
    selectShip.setAttribute("id", "selectShip")

    let distance = document.createElement("div")
    distance.className = "mdl-cell mdl-cell--3-col"
    distance.setAttribute("id", "distance")

    let cost = document.createElement("div")
    cost.className = "mdl-cell mdl-cell--3-col"
    cost.setAttribute("id", "cost")

    let time = document.createElement("div")
    time.className = "mdl-cell mdl-cell--3-col"
    time.setAttribute("id", "time")

    let saveRoute = document.createElement("button")
    saveRoute.className = "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
    saveRoute.setAttribute("style", "width:100%")
    saveRoute.setAttribute("id", "saveRoute")
    saveRoute.setAttribute("onclick", "saveRoute()")
    saveRoute.innerText = "SAVE THE ROUTE"

    grid.appendChild(selectShip)
    grid.appendChild(distance)
    grid.appendChild(cost)
    grid.appendChild(time)
    grid.appendChild(saveRoute)

    selectInRangeShip()
    distanceBetweenPort()
    displayCostAndTime(false)
}

// Drop down List for the Ship
function selectInRangeShip() {

    let selectShipRef = document.getElementById("selectShip")
    selectShipRef.innerHTML = "<b>Ship</b>" + "</br>" + "</br>"
    let selectShipButton = document.createElement("button")
    selectShipButton.className = "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
    selectShipButton.setAttribute("onclick", "shipOption()")
    selectShipButton.innerText = "Select a ship"
    selectShipRef.appendChild(selectShipButton)

}

// Get retrieve the ship on the list from API and User's Created
function shipOption() {
    if (typeof (Storage) !== undefined) {

        onclicked = true
        let shipOptionList = []
        let selectShipRef = document.getElementById("selectShip")
        selectShipRef.innerHTML = "<b>Ship</b>" + "</br>" + "</br>"

        let line = turf.lineString(allLocation);
        let length = turf.length(line, { units: 'kilometres' });

        let shipPDOList = JSON.parse(localStorage.getItem(shipListKey))
        if (shipPDOList !== null) {
            shipPDOList = shipPDOList._shipList

            for (let i = 0; i < shipPDOList.length; i++) {
                if (Number(shipPDOList[i]._range) >= length && shipPDOList[i]._status === "available") {
                    shipOptionList.push(shipPDOList[i]._shipName)
                }
            }
        }

        let shipAPIList = JSON.parse(localStorage.getItem(shipAPIkey)).ships
        for (let i = 0; i < shipAPIList.length; i++) {
            if (shipAPIList[i].range >= length && shipAPIList[i].status === "available") {
                shipOptionList.push(shipAPIList[i].name)
            }
        }

        // Drop down list
        let selectShipOption = document.createElement("select")
        selectShipOption.className = "mdl-textfield__input"
        selectShipOption.setAttribute("id", "selectShipOption")
        selectShipOption.setAttribute("onchange", "displayCostAndTime(true)")
        selectShipOption.innerHTML = "<option></option>"

        for (let i = 0; i < shipOptionList.length; i++) {
            let opt = document.createElement('option');
            opt.appendChild(document.createTextNode(shipOptionList[i]));
            opt.value = shipOptionList[i]
            selectShipOption.appendChild(opt)
        }

        selectShipRef.appendChild(selectShipOption)
    }
    else {
        alert("Local storage is unavailable")
    }
}

// Display the distance of shipping
function distanceBetweenPort() {

    let distanceRef = document.getElementById("distance")
    distanceRef.innerHTML = "<b>Distance</b>" + "</br>" + "</br>"

    let line = turf.lineString(allLocation);
    let length = turf.length(line, { units: 'kilometres' });
    distanceRef.innerHTML += length.toFixed(0) + "km"
    distanceRef.value = length.toFixed(0)
    if (onclicked === true) {
        shipOption()
        displayCostAndTime(true)
    }

}

// Display cost and time of shipping
function displayCostAndTime(check) {
    if (typeof (Storage) !== undefined) {

        let name = null
        if (check) {
            let selectShipOptionRef = document.getElementById("selectShipOption")
            name = selectShipOptionRef.value
        }

        let costRef = document.getElementById("cost")
        costRef.innerHTML = "<b>Cost</b>" + "</br>" + "</br>"
        let timeRef = document.getElementById("time")
        timeRef.innerHTML = "<b>Time</b>" + "</br>" + "</br>"

        if (name === null || name === "") {
            costRef.innerHTML += "Select a ship first"
            timeRef.innerHTML += "Select a ship first"
        } else {

            let line = turf.lineString(allLocation);
            let length = turf.length(line, { units: 'kilometres' });

            let shipAPIList = JSON.parse(localStorage.getItem(shipAPIkey)).ships
            for (let i = 0; i < shipAPIList.length; i++) {
                if (shipAPIList[i].name === name) {

                    costRef.innerHTML += "$" + (shipAPIList[i].cost * length).toFixed(0)
                    costRef.value = (shipAPIList[i].cost * length).toFixed(0)

                    let hours = length / (shipAPIList[i].maxSpeed * 1.852)
                    let days = hours / 24
                    timeRef.innerHTML += hours.toFixed(0) + " hours (" + days.toFixed(0) + " days)"
                    timeRef.value = hours.toFixed(0)
                }
            }

            let shipPDOList = JSON.parse(localStorage.getItem(shipListKey))
            if (shipPDOList !== null) {
                shipPDOList = shipPDOList._shipList

                for (let i = 0; i < shipPDOList.length; i++) {
                    if (shipPDOList[i]._shipName === name) {
                        costRef.innerHTML += "$" + (Number(shipPDOList[i]._cost) * length).toFixed(0)
                        costRef.value = (Number(shipPDOList[i]._cost) * length).toFixed(0)

                        let hours = length / (Number(shipPDOList[i]._maxSpeed) * 1.852)
                        let days = hours / 24
                        timeRef.innerHTML += hours.toFixed(0) + " hours (" + days.toFixed(0) + " days)"
                        timeRef.value = hours.toFixed(0)
                    }
                }
            }
        }
    }
    else {
        alert("Local storage is unavailable")
    }
}
// Save the Route to Local Storage and push in to class Route
function saveRoute() {
    if (typeof (Storage) !== undefined) {

        let ship = null
        let sourcePort = null
        let destinationPort = null
        let distance = null
        let time = null
        let cost = null
        let startDate = null

        let newRouteList = new RouteList()
        let routePDOList = JSON.parse(localStorage.getItem(routeListkey))
        if (routePDOList !== null) {
            newRouteList.initialiseFromRouteListPDO(routePDOList)
        }

        if (onclicked === true) {

            let routeName = prompt("Please input the route name")
            if (routeName === "" || routeName === null) {
                alert("Please enter an name for the route")
                return
            }
            else {
                let selectShipOptionRef = document.getElementById("selectShipOption")

                if (selectShipOptionRef.value) {

                    //Changing the ship status to en route for API ship and user created ship
                    let shipAPIList = JSON.parse(localStorage.getItem(shipAPIkey))
                    for (let i = 0; i < shipAPIList.ships.length; i++) {
                        if (shipAPIList.ships[i].name === selectShipOptionRef.value) {
                            shipAPIList.ships[i].status = "en-route"
                            localStorage.setItem(shipAPIkey, JSON.stringify(shipAPIList))
                            ship = shipAPIList.ships[i]
                        }
                    }
                    if (ship === null) {
                        let shipPDOList = JSON.parse(localStorage.getItem(shipListKey))
                        for (let i = 0; i < shipPDOList._shipList.length; i++) {
                            if (shipPDOList._shipList[i]._shipName === selectShipOptionRef.value) {
                                shipPDOList._shipList[i]._status = "en-route"
                                localStorage.setItem(shipListKey, JSON.stringify(shipPDOList))
                                ship = shipPDOList._shipList[i]
                            }
                        }
                    }

                    //Pushing the selcted port into array
                    let fromPortRef = document.getElementById("fromPort")
                    let portAPIList = JSON.parse(localStorage.getItem(portAPIkey)).ports
                    for (let i = 0; i < portAPIList.length; i++) {
                        if (portAPIList[i].name === fromPortRef.value) {
                            sourcePort = portAPIList[i]
                        }
                    }
                    if (sourcePort === null) {
                        let portUserList = JSON.parse(localStorage.getItem(portListKey))._portList
                        for (let i = 0; i < portUserList.length; i++) {
                            if (portUserList[i]._portName === fromPortRef.value) {
                                sourcePort = portUserList[i]
                            }
                        }
                    }

                    //Destination port
                    let toPortRef = document.getElementById("toPort")
                    for (let i = 0; i < portAPIList.length; i++) {
                        if (portAPIList[i].name === toPortRef.value) {
                            destinationPort = portAPIList[i]
                        }
                    }
                    if (destinationPort === null) {
                        let portUserList = JSON.parse(localStorage.getItem(portListKey))._portList
                        for (let i = 0; i < portUserList.length; i++) {
                            if (portUserList[i]._portName === toPortRef.value) {
                                destinationPort = portUserList[i]
                            }
                        }
                    }

                    //Distance
                    let distanceRef = document.getElementById("distance")
                    distance = distanceRef.value

                    //Time
                    let timeRef = document.getElementById("time")
                    time = timeRef.value

                    //Cost
                    let costRef = document.getElementById("cost")
                    cost = costRef.value

                    //Start date 
                    let startDateRef = document.getElementById("startDate")
                    startDate = startDateRef.value

                    let newRoute = new Route(routeName, ship, sourcePort, destinationPort, distance, time, cost, startDate, allLocation)
                    newRouteList.addToList(newRoute)
                    localStorage.setItem(routeListkey, JSON.stringify(newRouteList))
                    window.location.href = "index.html"

                }
                else {
                    alert("Please select the ship first!")
                }
            }


        }
        else {
            alert("Please select the ship first!")
        }
    }
    else {
        alert("Local storage is unavailable")
    }
}