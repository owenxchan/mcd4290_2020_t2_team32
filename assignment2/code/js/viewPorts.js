// Function creates a drop down list of countries
function dropDownListCountries() {
    if (typeof (Storage) !== undefined) {
        let portAPIList = JSON.parse(localStorage.getItem(portAPIkey))
        let portUserList = JSON.parse(localStorage.getItem(portListKey))

        let countryList = []

        if (portAPIList !== null) {
            for (let i = 0; i < portAPIList.ports.length; i++) {
                if (!(countryList.includes(portAPIList.ports[i].country))) {
                    countryList.push(portAPIList.ports[i].country)
                }
            }
        }

        if (portUserList !== null) {
            for (let i = 0; i < portUserList._portList.length; i++) {
                if (!(countryList.includes(portUserList._portList[i]._portCountry))) {
                    countryList.push(portUserList._portList[i]._portCountry)
                }
            }
        }
        countryList.sort()
        // assigning the null at the end of the list to Others
        countryList[countryList.length - 1] = "Other"

        let selectList = document.getElementById("dropDownList-countries")
        selectList.innerHTML = "<option></option>"

        for (let i = 0; i < countryList.length; i++) {
            let opt = document.createElement('option');
            opt.appendChild(document.createTextNode(countryList[i]));
            opt.value = countryList[i]
            selectList.appendChild(opt)
        }
    }
    else {
        alert("Local storage is unavailable")
    }

}

// Displays Ports after choosing a country 
function countryPorts() {
    if (typeof (Storage) !== undefined) {

        // getting the country selected by user
        let selectedCountry = document.getElementById("dropDownList-countries")
        let selectedCountryValue = selectedCountry.value

        if (selectedCountryValue === "Other") {
            selectedCountryValue = null
        }

        // reading out API and user created data from the local storage
        let portAPIList = JSON.parse(localStorage.getItem(portAPIkey))
        let portUserList = JSON.parse(localStorage.getItem(portListKey))

        let portAPIOutput = []
        let portUserOutput = []

        if (portAPIList !== null) {
            for (let i = 0; i < portAPIList.ports.length; i++) {
                if (portAPIList.ports[i].country === selectedCountryValue) {
                    portAPIOutput.push(portAPIList.ports[i])
                }
            }
        }


        if (portUserList !== null) {
            for (let i = 0; i < portUserList._portList.length; i++) {
                if (portUserList._portList[i]._portCountry === selectedCountryValue) {
                    portUserOutput.push(portUserList._portList[i])
                }
            }
        }

        // sorts the ports in the alphabetical order
        portAPIOutput.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        portUserOutput.sort((a, b) => (a._portName > b._portName) ? 1 : ((b._portName > a._portName) ? -1 : 0))

        // getting element by id 
        let cardContainerPortRef = document.getElementById("card-container-port")
        cardContainerPortRef.innerHTML = ""
        let card = null
        let cardTitle = null
        let cardText = null
        let portTitle = null
        let portSupportingText = null

        // Display for User card
        if (portUserOutput !== undefined) {
            //creating a loop for user ports and displays 
            for (let i = 0; i < portUserOutput.length; i++) {

                card = document.createElement("div")
                card.className = "demo-card-event mdl-card mdl-shadow--2dp"
                card.style.float = "left"

                // Display title name on Card
                cardTitle = document.createElement("div")
                cardTitle.className = "mdl-card__title mdl-card--expand"
                cardTitle.setAttribute("id", "Title" + i)

                // Display title support name on Card
                cardText = document.createElement("div")
                cardText.className = "mdl-card__supporting-text"
                cardText.setAttribute("id", "SupportingText" + i)

                card.appendChild(cardTitle)
                card.appendChild(cardText)
                cardContainerPortRef.appendChild(card)

                portTitle = document.getElementById("Title" + i)
                portSupportingText = document.getElementById("SupportingText" + i)

                // Display information on Card

                if (portUserOutput[i]._portCountry !== null) {
                    portTitle.innerHTML = "<h5>" + portUserOutput[i]._portName + ", " + "</br>" + portUserOutput[i]._portCountry + "</h5>"
                } else {
                    portTitle.innerHTML = "<h5>" + portUserOutput[i]._portName + "</h5>"
                }

                portSupportingText.innerHTML += "Type: " + portUserOutput[i]._portType + "</br>"
                portSupportingText.innerHTML += "Size: " + portUserOutput[i]._portSize + "</br>"
                portSupportingText.innerHTML += "Location precision: " + portUserOutput[i]._portLocPrecision + "</br>"
                portSupportingText.innerHTML += "Latitude: " + portUserOutput[i]._portLatitude + "</br>"
                portSupportingText.innerHTML += "Longitude: " + portUserOutput[i]._portLongitude + "</br>"
                portSupportingText.innerHTML += "Created by user" + "</br>"

            }
        }

        // Display for API card
        if (portAPIOutput !== undefined) {
            // creating a loop for API data and displays 
            for (let i = 0; i < portAPIOutput.length; i++) {
                let index = portUserOutput.length + i
                card = document.createElement("div")
                card.className = "demo-card-event mdl-card mdl-shadow--2dp"
                card.style.float = "left"

                // Display title name on Card
                cardTitle = document.createElement("div")
                cardTitle.className = "mdl-card__title mdl-card--expand"
                cardTitle.setAttribute("id", "Title" + index)

                // Display title support name on Card
                cardText = document.createElement("div")
                cardText.className = "mdl-card__supporting-text"
                cardText.setAttribute("id", "SupportingText" + index)

                card.appendChild(cardTitle)
                card.appendChild(cardText)
                cardContainerPortRef.appendChild(card)

                portTitle = document.getElementById("Title" + index)
                portSupportingText = document.getElementById("SupportingText" + index)

                // Display information on card
                if (portAPIOutput[i].country !== null) {
                    portTitle.innerHTML = "<h5>" + portAPIOutput[i].name + ", " + "</br>" + portAPIOutput[i].country + "</h5>"
                } else {
                    portTitle.innerHTML = "<h5>" + portAPIOutput[i].name + "</h5>"
                }

                portSupportingText.innerHTML += "Type: " + portAPIOutput[i].type + "</br>"
                portSupportingText.innerHTML += "Size: " + portAPIOutput[i].size + "</br>"
                portSupportingText.innerHTML += "Location precision: " + portAPIOutput[i].locprecision + "</br>"
                portSupportingText.innerHTML += "Latitude: " + portAPIOutput[i].lat + "</br>"
                portSupportingText.innerHTML += "Longitude: " + portAPIOutput[i].lng + "</br>"
                portSupportingText.innerHTML += "Got the data from API" + "</br>"
            }
        }
    }
    else {
        alert("Local strage is unavailable")
    }

}