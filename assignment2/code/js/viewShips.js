let editKey = "SHIP_EDIT_INDEX"

// function used to display user created ship
function displayInfoShips() {
    if (typeof (Storage) !== undefined) {

        // Get data from LocalStorage
        let newShipList = new ShipList()
        let shipPDOList = JSON.parse(localStorage.getItem(shipListKey))
        if (shipPDOList !== null) {

            newShipList.initialiseFromShipListPDO(shipPDOList)

            let cardContainer = document.getElementById("card-container")
            let card = null
            let cardTitle = null
            let cardText = null
            let acttionBar = null
            let editButton = null
            let editIcon = null
            let deleteButton = null
            let deleteIcon = null
            let title = null
            let supportingText = null

            // Create a loop to display multiple card and information for each card

            for (let count = 0; count < newShipList._shipList.length; count++) {
                // creating a div element in HTML
                card = document.createElement("div")
                card.className = "demo-card-event mdl-card mdl-shadow--2dp"
                card.style.float = "left"

                // Display title name on Card
                cardTitle = document.createElement("div")
                cardTitle.className = "mdl-card__title mdl-card--expand"
                cardTitle.setAttribute("id", "title" + count)

                // DIsplay support text on Card
                cardText = document.createElement("div")
                cardText.className = "mdl-card__supporting-text"
                cardText.setAttribute("id", "supportingText" + count)

                acttionBar = document.createElement("div")
                acttionBar.className = "mdl-card__actions mdl-card--border"

                // Create the Edit button
                editButton = document.createElement("BUTTON")
                editButton.className = "mdl-button mdl-js-button mdl-button--icon"
                editIcon = document.createElement("i")
                editIcon.className = "material-icons"
                editIcon.setAttribute("onclick", "editShip(" + count + ")")
                editIcon.innerHTML = "edit"

                // Create the Delete button
                deleteButton = document.createElement("BUTTON")
                deleteButton.className = "mdl-button mdl-js-button mdl-button--icon"
                deleteIcon = document.createElement("i")
                deleteIcon.className = "material-icons"
                deleteIcon.setAttribute("onclick", "deleteShip(" + count + ")")
                deleteIcon.innerHTML = "delete"

                // closing all the created HTML element
                card.appendChild(cardTitle)
                card.appendChild(cardText)
                editButton.appendChild(editIcon)
                deleteButton.appendChild(deleteIcon)
                acttionBar.appendChild(editButton)
                acttionBar.appendChild(deleteButton)
                card.appendChild(acttionBar)
                cardContainer.appendChild(card)

                // Display all information on Card
                title = document.getElementById("title" + count)
                supportingText = document.getElementById("supportingText" + count)

                title.innerHTML = "<h5>" + newShipList._shipList[count].shipName + " @ $" + newShipList._shipList[count].cost + " per km" + "<h5>"

                supportingText.innerHTML += "Max Speed: " + newShipList._shipList[count].maxSpeed + "Knots" + "</br>"
                supportingText.innerHTML += "Range: " + newShipList._shipList[count].range + "Km" + "</br>"
                supportingText.innerHTML += "Description: " + newShipList._shipList[count].description + "</br>"
                supportingText.innerHTML += "Status: " + newShipList._shipList[count].status + "</br>"
                supportingText.innerHTML += "Created by: user" + "</br>"
            }
        }
    }
    else {
        alert("Local storage is unavailable")
    }
}

//function to display Ship from API
function displayAPIship() {
    if (typeof (Storage) !== undefined) {

        // rwading out API ship data from local storage 
        let shipAPIList = JSON.parse(localStorage.getItem(shipAPIkey)).ships
        let APIcardContainerRef = document.getElementById("APIShip-card-container")
        let card = null
        let cardTitle = null
        let cardText = null
        let shipTitle = null
        let shipSupportingText = null

        for (let i = 0; i < shipAPIList.length; i++) {
            card = document.createElement("div")
            card.className = "demo-card-event mdl-card mdl-shadow--2dp"
            card.style.float = "left"

            // Display title name on Card
            cardTitle = document.createElement("div")
            cardTitle.className = "mdl-card__title mdl-card--expand"
            cardTitle.setAttribute("id", "APITitle" + i)

            // DIsplay support text on Card
            cardText = document.createElement("div")
            cardText.className = "mdl-card__supporting-text"
            cardText.setAttribute("id", "APISupportingText" + i)

            // closing all the created HTML element
            card.appendChild(cardTitle)
            card.appendChild(cardText)
            APIcardContainerRef.appendChild(card)

            shipTitle = document.getElementById("APITitle" + i)
            shipSupportingText = document.getElementById("APISupportingText" + i)

            // Display all information on Card
            shipTitle.innerHTML = "<h5>" + shipAPIList[i].name + " @ $" + shipAPIList[i].cost + " per km" + "</h5>"

            shipSupportingText.innerHTML += "Max Speed: " + shipAPIList[i].maxSpeed + "Knots" + "</br>"
            shipSupportingText.innerHTML += "Range: " + shipAPIList[i].range + "km" + "</br>"
            shipSupportingText.innerHTML += "Description: " + shipAPIList[i].desc + "</br>"
            shipSupportingText.innerHTML += "Status: " + shipAPIList[i].status + "</br>"
            shipSupportingText.innerHTML += "Got the data from API" + "</br>"
        }
    }
    else {
        alert("Local storage is unavailable")
    }
}
// Edit button funtion
function editShip(count) {
    if (typeof (Storage) !== undefined) {
        // saving the index the user wants to save
        let index = count
        localStorage.setItem(editKey, index)
        window.location.href = "editShip.html";
    }
    else {
        alert("Local storage is unavailable")
    }

}

// Displat the info in edit.html
function editDisplay() {
    if (typeof (Storage) !== undefined) {
        //Retrive data from the Localstorage
        let retrivedObject = (JSON.parse(localStorage.getItem(shipListKey)))
        let newShipList = new ShipList()
        newShipList.initialiseFromShipListPDO(retrivedObject)
        let index = localStorage.getItem(editKey)

        // displaying the created ships data in the texttbox 
        let shipName = document.getElementById("NameEdit")
        shipName.value = newShipList.shipList[index].shipName

        let maxSpeed = document.getElementById("MaxSpeedEdit")
        maxSpeed.value = newShipList.shipList[index].maxSpeed

        let travelRange = document.getElementById("RangeEdit")
        travelRange.value = newShipList.shipList[index].range

        let description = document.getElementById("DescriptionEdit")
        description.value = newShipList.shipList[index].description

        let cost = document.getElementById("CostEdit")
        cost.value = newShipList.shipList[index].cost

        let status = document.getElementById("StatusEdit")
        status.value = newShipList.shipList[index].status
    } else {
        alert("Local storage is unavailable")
    }

}

//Update button ship function
function updateShip() {

    if (typeof (Storage) !== 'undefined') {
        let index = localStorage.getItem(editKey)
        let retrivedObject = JSON.parse(localStorage.getItem(shipListKey))
        let newShipList = new ShipList()
        newShipList.initialiseFromShipListPDO(retrivedObject)

        //Detele the old data
        newShipList._shipList.splice(index, 1)
        localStorage.setItem(shipListKey, JSON.stringify(newShipList))


        // Get data from HTML (Update)
        let name = document.getElementById("NameEdit").value
        let maximumSpeed = document.getElementById("MaxSpeedEdit").value
        let range = document.getElementById("RangeEdit").value
        let description = document.getElementById("DescriptionEdit").value
        let cost = document.getElementById("CostEdit").value
        let status = document.getElementById("StatusEdit").value

        if (name !== "" && maximumSpeed !== "" && range !== "" && description !== "" && cost) {

            // Save data in Local Storage
            let newShip = new Ship(name, maximumSpeed, range, description, cost)
            newShip.status = status
            newShipList.addToList(newShip)
            localStorage.setItem(shipListKey, JSON.stringify(newShipList))
            window.location.href = "viewShips.html"// directs the user to view ship page to see the updates
        }
        else {
            alert("Please enter all required information!")
        }

    }
    else {
        alert('localStorage is not supported by current browser.')
    }
}

//Function for delete button
function deleteShip(count) {

    if (typeof (Storage) !== undefined) {

        let retrivedObject = (JSON.parse(localStorage.getItem(shipListKey)))
        let newShipList = new ShipList()
        newShipList.initialiseFromShipListPDO(retrivedObject)

        //Delete the ship card
        newShipList._shipList.splice(count, 1)
        localStorage.setItem(shipListKey, JSON.stringify(newShipList))
        location.reload();
    }
    else {
        alert("Local storage is unavailable")
    }

}