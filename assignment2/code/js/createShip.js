// Making a Create Ship funtion
function createShip() {
    if (typeof (Storage) !== 'undefined') {

        // creating a public dummy object to hold the information that is read out.
        let newShipList = new ShipList()
        let shipPDOList = JSON.parse(localStorage.getItem(shipListKey))
        if (shipPDOList !== null) {
            newShipList.initialiseFromShipListPDO(shipPDOList)
        }

        // getting Element  by id from HTML
        let nameShipRef = document.getElementById("Name")
        let maximumSpeedShipRef = document.getElementById("MaxSpeed")
        let rangeShipRef = document.getElementById("Range")
        let descriptionShipRef = document.getElementById("Description")
        let costShipRef = document.getElementById("Cost")

        let nameShipValue = nameShipRef.value
        let maximumSpeedShipValue = maximumSpeedShipRef.value
        let rangeShipValue = rangeShipRef.value
        let descriptionShipValue = descriptionShipRef.value
        let costShipValue = costShipRef.value

        if (nameShipValue !== "" && maximumSpeedShipValue !== "" && rangeShipValue !== "" && descriptionShipValue !== "" && costShipValue !== "") {

            // Save data to the class Ship to save in the LocalStorage
            let newShip = new Ship(nameShipValue, maximumSpeedShipValue, rangeShipValue, descriptionShipValue, costShipValue)
            newShipList.addToList(newShip)
            localStorage.setItem(shipListKey, JSON.stringify(newShipList))
            window.location.href = "viewShips.html" // directs the user to view ship page
        }
        else {
            alert("Please enter all required information!")
        }

    }
    else {
        alert("Local storage is unavailable")
    }
}