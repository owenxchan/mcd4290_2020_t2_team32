// Get data from API dynamically
function APIRequest() {

    if (typeof (Storage) !== undefined) {
        let portAPIList = JSON.parse(localStorage.getItem(portAPIkey))
        let shipAPIList = JSON.parse(localStorage.getItem(shipAPIkey))

        if (portAPIList === null & shipAPIList === null) {
            let shipAPIUrl = "https://eng1003.monash/api/v1/ships/?callback=APIshipResponse"
            let portAPIUrl = "https://eng1003.monash/api/v1/ports/?callback=APIportResponse"

            let scriptShip = document.createElement('script');
            scriptShip.src = shipAPIUrl
            document.body.appendChild(scriptShip);

            let scriptPort = document.createElement('script');
            scriptPort.src = portAPIUrl
            document.body.appendChild(scriptPort);
        }
    }
    else {
        alert("Local storage is unavailable")
    }

}

function APIportResponse(APIList) {
    // saving the port data to local storage
    if (typeof (Storage) !== undefined) {
        localStorage.setItem(portAPIkey, JSON.stringify(APIList))
    }
    else {
        alert("Local storage is unavailable")
    }
}

function APIshipResponse(APIList) {
    // saving the ship data to local storage
    if (typeof (Storage) !== undefined) {
        localStorage.setItem(shipAPIkey, JSON.stringify(APIList))
    }
    else {
        alert("Local storage is unavailable")
    }
}

// Display the date and calculate time for traveling.
function displayRoutes() {
    let ongoingCheck = false
    let pastCheck = false
    let upcomingCheck = false
    // reading out data
    if (typeof (Storage) !== undefined) {
        let routePODList = JSON.parse(localStorage.getItem(routeListkey))

        // creating dummy object to use methods
        if (routePODList !== null) {

            let newRouteList = new RouteList()
            newRouteList.initialiseFromRouteListPDO(routePODList)

            // creating a loop to sort rout list data in order of upcomming, future and past trips  
            for (let count = 0; count < newRouteList.routeList.length; count++) {

                let newRoute = new Route()
                newRoute.initialiseFromRoutePDO(newRouteList.routeList[count])


                let startDate = new Date(newRoute.startDate)
                let endDate = new Date(newRoute.startDate)
                endDate.setDate(startDate.getDate() + Math.ceil((newRoute.time) / 24));
                let today = new Date()

                if (startDate <= today && endDate >= today) {

                    onGoingTrip(count)
                    ongoingCheck = true

                }
                else if (endDate <= today) {
                    pastTrip(count)
                    pastCheck = true
                }
                else {
                    upcomingTrip(count)
                    upcomingCheck = true
                }
            }
        }

        if (!ongoingCheck) {
            let cardContainer = document.getElementById("card-container-ongoing")
            let message = document.createElement("div")
            message.className = "mdl-cell mdl-cell--2-col"
            message.innerHTML = "No route available"
            cardContainer.appendChild(message)
        }

        if (!pastCheck) {
            let cardContainer = document.getElementById("card-container-past")
            let message = document.createElement("div")
            message.className = "mdl-cell mdl-cell--2-col"
            message.innerHTML = "No route available"
            cardContainer.appendChild(message)
        }

        if (!upcomingCheck) {
            let cardContainer = document.getElementById("card-container-upcoming")
            let message = document.createElement("div")
            message.className = "mdl-cell mdl-cell--2-col"
            message.innerHTML = "No route available"
            cardContainer.appendChild(message)
        }

    }
    else {
        alert("Local storage is unavailable")
    }

}

// Displaying information
function onGoingTrip(count) {
    if (typeof (Storage) !== undefined) {

        let newRouteList = new RouteList()
        let routePODList = JSON.parse(localStorage.getItem(routeListkey))
        newRouteList.initialiseFromRouteListPDO(routePODList)

        let newRoute = new Route()
        newRoute.initialiseFromRoutePDO(newRouteList.routeList[count])

        let cardContainer = document.getElementById("card-container-ongoing")
        let card = null
        let cardTitle = null
        let cardText = null
        let acttionBar = null
        let acttionBarButton = null
        let title = null
        let supportingText = null

        card = document.createElement("div")
        card.className = "demo-card-event mdl-card mdl-shadow--2dp"
        card.style.float = "left"

        cardTitle = document.createElement("div")
        cardTitle.className = "mdl-card__title mdl-card--expand"
        cardTitle.setAttribute("id", "title" + count)

        cardText = document.createElement("div")
        cardText.className = "mdl-card__supporting-text"
        cardText.setAttribute("id", "supportingText" + count)

        acttionBar = document.createElement("div")
        acttionBar.className = "mdl-card__actions mdl-card--border"
        acttionBar.setAttribute("onclick", "moreInfoButton(" + count + ")")

        acttionBarButton = document.createElement("a")
        acttionBarButton.className = "mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"

        acttionBar.appendChild(acttionBarButton)

        card.appendChild(cardTitle)
        card.appendChild(cardText)
        card.appendChild(acttionBar)
        cardContainer.appendChild(card)
        title = document.getElementById("title" + count)
        supportingText = document.getElementById("supportingText" + count)

        title.innerHTML = "Route name: " + newRoute.routeName
        supportingText.innerHTML += "Total distance: " + newRoute.distance + " km" + "</br>"
        supportingText.innerHTML += "Cost: " + "$" + newRoute.routeCost + "</br>"
        supportingText.innerHTML += "Estimated time: " + newRoute.time + " hr" + "</br>"
        supportingText.innerHTML += "Estimated days: " + (newRoute.time / 24).toFixed(0) + " days" + "</br>"
        supportingText.innerHTML += "Start date: " + newRoute.startDate + "</br>"
        acttionBarButton.innerText = "For More Information Click Here"
    }
    else {
        alert("Local storage is unavailable")
    }

}

// Display information of Route with trip will shipped
function upcomingTrip(count) {

    if (typeof (Storage) !== undefined) {

        let newRouteList = new RouteList()
        let routePODList = JSON.parse(localStorage.getItem(routeListkey))
        newRouteList.initialiseFromRouteListPDO(routePODList)

        let newRoute = new Route()
        newRoute.initialiseFromRoutePDO(newRouteList.routeList[count])

        let cardContainer = document.getElementById("card-container-upcoming")
        let card = null
        let cardTitle = null
        let cardText = null
        let acttionBar = null
        let acttionBarButton = null
        let title = null
        let supportingText = null

        card = document.createElement("div")
        card.className = "demo-card-event mdl-card mdl-shadow--2dp"
        card.style.float = "left"

        cardTitle = document.createElement("div")
        cardTitle.className = "mdl-card__title mdl-card--expand"
        cardTitle.setAttribute("id", "title" + count)

        cardText = document.createElement("div")
        cardText.className = "mdl-card__supporting-text"
        cardText.setAttribute("id", "supportingText" + count)

        acttionBar = document.createElement("div")
        acttionBar.className = "mdl-card__actions mdl-card--border"
        acttionBar.setAttribute("onclick", "moreInfoButton(" + count + ")")

        acttionBarButton = document.createElement("a")
        acttionBarButton.className = "mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"

        acttionBar.appendChild(acttionBarButton)

        card.appendChild(cardTitle)
        card.appendChild(cardText)
        card.appendChild(acttionBar)
        cardContainer.appendChild(card)
        title = document.getElementById("title" + count)
        supportingText = document.getElementById("supportingText" + count)

        title.innerHTML = "Route name: " + newRoute.routeName
        supportingText.innerHTML += "Total distance: " + newRoute.distance + " km" + "</br>"
        supportingText.innerHTML += "Cost: " + "$" + newRoute.routeCost + "</br>"
        supportingText.innerHTML += "Estimated time: " + newRoute.time + " hr" + "</br>"
        supportingText.innerHTML += "Estimated days: " + (newRoute.time / 24).toFixed(0) + " days" + "</br>"
        supportingText.innerHTML += "Start date: " + newRoute.startDate + "</br>"
        acttionBarButton.innerText = "For More Information Click Here"
    }
    else {
        alert("Local Storage is unavailable ")
    }

}

// Display information of Route with trip is shipped
function pastTrip(count) {
    if (typeof (Storage) !== undefined) {
        let newRouteList = new RouteList()
        let routePODList = JSON.parse(localStorage.getItem(routeListkey))
        newRouteList.initialiseFromRouteListPDO(routePODList)

        let newRoute = new Route()
        newRoute.initialiseFromRoutePDO(newRouteList.routeList[count])

        let cardContainer = document.getElementById("card-container-past")
        let card = null
        let cardTitle = null
        let cardText = null
        let acttionBar = null
        let acttionBarButton = null
        let title = null
        let supportingText = null

        card = document.createElement("div")
        card.className = "demo-card-event mdl-card mdl-shadow--2dp"
        card.style.float = "left"

        cardTitle = document.createElement("div")
        cardTitle.className = "mdl-card__title mdl-card--expand"
        cardTitle.setAttribute("id", "title" + count)

        cardText = document.createElement("div")
        cardText.className = "mdl-card__supporting-text"
        cardText.setAttribute("id", "supportingText" + count)

        acttionBar = document.createElement("div")
        acttionBar.className = "mdl-card__actions mdl-card--border"
        acttionBar.setAttribute("onclick", "moreInfoButton(" + count + ")")

        acttionBarButton = document.createElement("a")
        acttionBarButton.className = "mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"

        acttionBar.appendChild(acttionBarButton)

        card.appendChild(cardTitle)
        card.appendChild(cardText)
        card.appendChild(acttionBar)
        cardContainer.appendChild(card)
        title = document.getElementById("title" + count)
        supportingText = document.getElementById("supportingText" + count)

        title.innerHTML = "Route name: " + newRoute.routeName
        supportingText.innerHTML += "Total distance: " + newRoute.distance + " km" + "</br>"
        supportingText.innerHTML += "Cost: " + "$" + newRoute.routeCost + "</br>"
        supportingText.innerHTML += "Estimated time: " + newRoute.time + " hr" + "</br>"
        supportingText.innerHTML += "Estimated days: " + (newRoute.time / 24).toFixed(0) + " days" + "</br>"
        supportingText.innerHTML += "Start date: " + newRoute.startDate + "</br>"
        acttionBarButton.innerHTML = "For More Information Click Here"
    }
    else {
        alert("Local storage is unavailable")
    }

}

// Move to View Route Page
function moreInfoButton(count) {
    if (typeof (Storage) !== undefined) {
        localStorage.setItem(moreInfoKey, count)
        window.location.href = "viewRoute.html"
    }
    else {
        alert("Local storage is unavailable")
    }

}

