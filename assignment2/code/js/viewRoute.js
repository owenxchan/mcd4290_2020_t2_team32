//displaying route information 
function viewRoute() {
    if (typeof (Storage) !== undefined) {
        let count = JSON.parse(localStorage.getItem(moreInfoKey))

        let newRouteList = new RouteList()
        let routePODList = JSON.parse(localStorage.getItem(routeListkey))
        newRouteList.initialiseFromRouteListPDO(routePODList)

        let newRoute = new Route()
        newRoute.initialiseFromRoutePDO(newRouteList.routeList[count])

        let grid = document.getElementById("showRoute")

        while (grid.hasChildNodes()) {
            grid.removeChild(grid.firstChild);
        }

        let shipName = document.createElement("div")
        shipName.className = "mdl-cell mdl-cell--2-col"

        let distance = document.createElement("div")
        distance.className = "mdl-cell mdl-cell--2-col"

        let cost = document.createElement("div")
        cost.className = "mdl-cell mdl-cell--2-col"

        let startDate = document.createElement("div")
        startDate.className = "mdl-cell mdl-cell--4-col"
        startDate.setAttribute("id", "startDate")

        let time = document.createElement("div")
        time.className = "mdl-cell mdl-cell--2-col"

        grid.appendChild(shipName)
        grid.appendChild(distance)
        grid.appendChild(cost)
        grid.appendChild(time)
        grid.appendChild(startDate)        

        shipName.innerHTML = "<b>Ship</b>" + "</br>" + "</br>"
        if (newRoute.ship.name !== undefined) {
            shipName.innerHTML += newRoute.ship.name
        } else {
            shipName.innerHTML += newRoute.ship._shipName
        }

        distance.innerHTML = "<b>Distance</b>" + "</br>" + "</br>"
        distance.innerHTML += newRoute.distance + " km"

        cost.innerHTML = "<b>Cost</b>" + "</br>" + "</br>"
        cost.innerHTML += "$" + newRoute.routeCost

        startDate.innerHTML = "<b>Start date</b>" + "</br>" + "</br>"
        startDate.innerHTML += newRoute.startDate

        time.innerHTML = "<b>Time</b>" + "</br>" + "</br>"
        time.innerHTML += (newRoute.time / 24).toFixed(0) + " day(s)"

        let startDateButton = document.createElement("button")
        startDateButton.className = "mdl-button mdl-js-button mdl-button--icon"
        let startDateIcon = document.createElement("i")
        startDateIcon.setAttribute("onclick", "changeDate(" + count + ")")
        startDateIcon.className = "material-icons"
        startDateIcon.innerHTML = "edit"
        startDateButton.appendChild(startDateIcon)
        startDate.appendChild(startDateButton)
    }
    else {
        alert("Local storage is unavailable")
    }

}

// function for changing the date
function changeDate(count) {
    if (typeof (Storage) !== undefined) {
        let startDateRef = document.getElementById("startDate")

        let newRouteList = JSON.parse(localStorage.getItem(routeListkey))
        let today = new Date()
        let startDate = new Date(newRouteList._routeList[count]._startDate)

        if (startDate > today) {
            startDateRef.removeChild(startDateRef.lastChild);
            let dateInput = document.createElement("input")
            dateInput.className = "mdl-textfield__input"
            dateInput.setAttribute("id", "startDateInput")
            dateInput.setAttribute("type", "date")
            dateInput.setAttribute("onchange", "dateUpdate(" + count + ")")
            startDateRef.appendChild(dateInput)
        }
        else {
            alert("The trip is already on the way or passed!")
        }
    }
    else {
        alert("Local storage is unavailable")
    }

}

function dateUpdate(count) {
    if (typeof (Storage) !== undefined) {
        let startDateInputRef = document.getElementById("startDateInput")
        let newRouteList = JSON.parse(localStorage.getItem(routeListkey))
        let orginalStartDate = new Date(newRouteList._routeList[count]._startDate)
        let startDate = new Date(startDateInputRef.value)

        if (startDate > orginalStartDate) {

            if (confirm("Do you want to continue? (Once you postpone the trip, you can only postpone the trip base on the new date)")) {
                newRouteList._routeList[count]._startDate = startDateInputRef.value
                localStorage.setItem(routeListkey, JSON.stringify(newRouteList))
            }
            viewRoute()
        }
        else {
            alert("You can only postpone the trip!")
        }
    }
    else {
        alert("Local storage is unavailable")
    }

}

// function for displaying map
function mapDisplay() {
    if (typeof (Storage) !== undefined) {

        mapboxgl.accessToken = 'pk.eyJ1IjoidG5ndTAxMTIiLCJhIjoiY2tjd3Jud21lMDU2NjJ0bG0xeWx0NDcwMSJ9.RFxXb37wxqSmRPqFD1ewvg';
        let count = JSON.parse(localStorage.getItem(moreInfoKey))

        let newRouteList = new RouteList()
        let routePODList = JSON.parse(localStorage.getItem(routeListkey))
        newRouteList.initialiseFromRouteListPDO(routePODList)

        let newRoute = new Route()
        newRoute.initialiseFromRoutePDO(newRouteList.routeList[count])

        let location = newRoute.wayPointList
        let sourceLocation = location[0]
        let desLocation = location[location.length - 1]

        let map = new mapboxgl.Map
            ({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v11',
                zoom: 1.5
            });

        // Source Port
        let sourcePort = newRoute.sourcePort.name

        if (sourcePort === undefined) {
            sourcePort = newRoute.sourcePort._portName + ", " + newRoute.sourcePort._portCountry
        } else {
            sourcePort += ", " + newRoute.sourcePort.country
        }

        let sourcePopup = new mapboxgl.Popup({ offset: 15 })
            .setHTML("Source port: </br>" + sourcePort)
            .addTo(map)

        let sourceMarker = new mapboxgl.Marker({ color: "green" })
            .setLngLat(sourceLocation)
            .addTo(map)
            .setPopup(sourcePopup);

        // Destination Port
        let desPort = newRoute.destinationPort.name

        if (desPort === undefined) {
            desPort = newRoute.destinationPort._portName + ", " + newRoute.destinationPort._portCountry
        } else {
            desPort += ", " + newRoute.destinationPort.country
        }

        let desPopup = new mapboxgl.Popup({ offset: 15 })
            .setHTML("Destination port: </br>" + desPort)
            .addTo(map)

        let desMarker = new mapboxgl.Marker({ color: "red" })
            .setLngLat(desLocation)
            .addTo(map)
            .setPopup(desPopup);

        if (location.length > 2) {

            for (let i = 1; i < location.length - 1; i++) {

                let Marker = new mapboxgl.Marker({ color: "orange" })
                    .setLngLat(location[i])
                    .addTo(map)
            }

        }

        map.on('load', function () {
            map.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': location
                    }
                }
            });

            map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#888',
                    'line-width': 8
                }
            });
        });

        map.panTo([(sourceLocation[0] + desLocation[0]) / 2, (sourceLocation[1] + desLocation[1]) / 2])
    }
    else {
        alert("Local storage is unavailable")
    }

}

function deleteRoute() {

    if (typeof (Storage) !== undefined) {

        if (confirm("Do you want to delete this Route?") === true) {

            let count = JSON.parse(localStorage.getItem(moreInfoKey))

            let newRouteList = new RouteList()
            let routePODList = JSON.parse(localStorage.getItem(routeListkey))
            newRouteList.initialiseFromRouteListPDO(routePODList)

            let newRoute = new Route()
            newRoute.initialiseFromRoutePDO(newRouteList.routeList[count])

            let startDate = new Date(newRoute.startDate)
            let endDate = new Date(newRoute.startDate)
            endDate.setDate(startDate.getDate() + Math.ceil((newRoute.time) / 24));
            let today = new Date()

            if (startDate <= today && endDate >= today) {
                alert("Trip is on the way!!!")

            } else {

                //Ship
                let ship = null
                let shipAPIList = JSON.parse(localStorage.getItem(shipAPIkey))
                for (let i = 0; i < shipAPIList.ships.length; i++) {
                    if (shipAPIList.ships[i].name === newRoute.ship.name) {
                        shipAPIList.ships[i].status = "available"
                        localStorage.setItem(shipAPIkey, JSON.stringify(shipAPIList))
                        ship = shipAPIList.ships[i]
                    }
                }
                if (ship === null) {
                    let shipPDOList = JSON.parse(localStorage.getItem(shipListKey))
                    for (let i = 0; i < shipPDOList._shipList.length; i++) {
                        if (shipPDOList._shipList[i]._shipName === newRoute.ship._shipName) {
                            shipPDOList._shipList[i]._status = "available"
                            localStorage.setItem(shipListKey, JSON.stringify(shipPDOList))
                        }
                    }
                }
                newRouteList.routeList.splice(count, 1)
                localStorage.setItem(routeListkey, JSON.stringify(newRouteList))
                window.location.href = "index.html"
            }
        }

    }
    else {
        alert("Local storage is unavailable")
    }

}