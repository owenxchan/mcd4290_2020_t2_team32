const portListKey = "PORT_LIST_STORAGE"
const shipListKey = "SHIP_LIST_STORAGE"
const portAPIkey = "PORT_API_STORAGE"
const shipAPIkey = "SHIP_API_STORAGE"
const routeListkey = "ROUTE_LIST_STORAGE"
const moreInfoKey = "MORE_INFO_KEY"

// ShipList Class =========================================

class Ship {
    constructor(shipName, maxSpeed, range, description, cost) {
        this._shipName = shipName;
        this._maxSpeed = maxSpeed;
        this._range = range;
        this._description = description;
        this._cost = cost;
        this._status = "available";
    }

    // Ship Name =============================       
    set shipName(Name) {
        this._shipName = Name;
    }
    get shipName() {
        return this._shipName
    }

    // Maximum Speed ========================        
    set maxSpeed(maxSpeed) {
        this._maxSpeed = maxSpeed;
    }
    get maxSpeed() {
        return this._maxSpeed
    }

    // Range ================================
    set range(range) {
        this._range = range;
    }
    get range() {
        return this._range
    }

    // Description ==========================
    set description(description) {
        this._description = description;
    }
    get description() {
        return this._description
    }

    //Cost ==================================
    set cost(cost) {
        this._cost = cost;
    }
    get cost() {
        return this._cost
    }

    //Status ===============================
    set status(status) {
        this._status = status;
    }
    get status() {
        return this._status
    }

    // Reinitialises this instance from a public-data port object.
    initialiseFromShipPDO(shipObject) {
        // Initialise the instance via the mutator methods from the PDO object.
        this._shipName = shipObject._shipName;
        this._maxSpeed = shipObject._maxSpeed;
        this._range = shipObject._range;
        this._description = shipObject._description;
        this._cost = shipObject._cost;
        this._status = shipObject._status;

    }


}


// Port Class =========================================

class Port {
    constructor(name, country, type, size, locPrecision, latitude, longitude) {
        this._portName = name;
        this._portCountry = country;
        this._portType = type;
        this._portSize = size;
        this._portLocPrecision = locPrecision;
        this._portLatitude = latitude;
        this._portLongitude = longitude;
    }

    // Name of the port ======================        
    set portName(name) {
        this._portName = name;
    }
    get portName() {
        return this._portName
    }

    // Country ==============================
    set portCountry(country) {
        this._portCountry = country;
    }
    get portCountry() {
        return this._portCountry
    }

    // Type of Port =========================
    set portType(type) {
        this._portType = type;
    }
    get portType() {
        return this._portType
    }

    // Size of Port =========================
    set portSize(size) {
        this._portSize = size;
    }
    get portSize() {
        return this._portSize
    }

    // Location Precision ====================
    set portLocPrecision(locPrecision) {
        this._portLocPrecision = locPrecision
    }

    get portLocPrecision() {
        return this._portLocPrecision
    }

    // Latitude ==============================
    set portLatitude(coordinates) {
        this._portLatitude = coordinates;
    }
    get portLatitude() {
        return this._portLatitude
    }

    // Longitude =============================
    set portLongitude(coordinates) {
        this._portLongitude = coordinates;
    }
    get portLongitude() {
        return this._portLongitude
    }

    // Reinitialises this instance from a public-data port object.
    initialiseFromPortPDO(portObject) {
        // Initialise the instance via the mutator methods from the PDO object.
        this._portName = portObject._portName;
        this._portCountry = portObject._portCountry;
        this._portType = portObject._portType;
        this._portSize = portObject._portSize;
        this._portLocPrecision = portObject._portLocPrecision;
        this._portLatitude = portObject._portLatitude;
        this._portLongitude = portObject._portLongitude;
    }

}


// Route List Class =========================================

class Route {


    constructor(routeName, ship, sourcePort, destinationPort, distance, time, routeCost, startDate, wayPointList) {
        this._routeName = routeName;
        this._ship = ship;
        this._sourcePort = sourcePort;
        this._destinationPort = destinationPort;
        this._distance = distance;
        this._time = time;
        this._routeCost = routeCost;
        this._startDate = startDate;
        this._wayPointList = wayPointList;
    }

    //Route Name ======================================
    set routeName(Name) {
        this._routeName = Name;
    }
    get routeName() {
        return this._routeName
    }

    //Shipping route ==================================
    set ship(Route) {
        this._ship = Route;
    }
    get ship() {
        return this._ship
    }

    //Source Port ====================================
    set sourcePort(Port) {
        this._sourcePort = Port;
    }
    get sourcePort() {
        return this._sourcePort
    }

    //DestinationPort =================================
    set destinationPort(Port) {
        this._destinationPort = Port;
    }
    get destinationPort() {
        return this._destinationPort
    }

    //Distance of travel ==============================
    set distance(distance) {
        this._distance = distance;
    }
    get distance() {
        return this._distance
    }

    //Time =============================================
    set time(time) {
        this._time = time;
    }
    get time() {
        return this._time
    }

    //route cost =======================================
    set routeCost(Cost) {
        this._routeCost = Cost;
    }
    get routeCost() {
        return this._routeCost;
    }

    //Beginning Date ====================================
    set startDate(Date) {
        this._startDate = Date;
    }
    get startDate() {
        return this._startDate;
    }

    //Way Point List ====================================
    set wayPointList(wayPointList) {
        this._wayPointList = wayPointList;
    }
    get wayPointList() {
        return this._wayPointList;
    }
    addToWaypoint(lngLat) {
        this._wayPointList.push(lngLat)
    }
    // Reinitialises this instance from a public-data port object.
    initialiseFromRoutePDO(routeObject) {
        // Initialise the instance via the mutator methods from the PDO object.
        this._routeName = routeObject._routeName;
        this._ship = routeObject._ship;
        this._sourcePort = routeObject._sourcePort;
        this._destinationPort = routeObject._destinationPort;
        this._distance = routeObject._distance;
        this._time = routeObject._time;
        this._routeCost = routeObject._routeCost;
        this._startDate = routeObject._startDate;
        this._wayPointList = routeObject._wayPointList;
    }
}


class ShipList {
    constructor() {
        this._shipList = []
    }
    addToList(value) {
        this._shipList.push(value)
    }

    get shipList() {
        return this._shipList
    }

    initialiseFromShipListPDO(shipListPODObject) {
        for (let k = 0; k < shipListPODObject._shipList.length; k++) {
            let newShip = new Ship();
            newShip.initialiseFromShipPDO(shipListPODObject._shipList[k]);
            this._shipList[k] = newShip;
        }
    }

}

class PortList {
    constructor() {
        this._portList = []
    }
    addToList(value) {
        this._portList.push(value)
    }
    get portList() {
        return this._portList
    }

    initialiseFromPortListPDO(portListPODObject) {
        for (let i = 0; i < portListPODObject._portList.length; i++) {
            let newPort = new Port();
            newPort.initialiseFromPortPDO(portListPODObject._portList[i]);
            this._portList[i] = newPort;
        }
    }
}

class RouteList {
    constructor() {
        this._routeList = []
    }
    addToList(value) {
        this._routeList.push(value)
    }
    get routeList() {
        return this._routeList
    }
    initialiseFromRouteListPDO(routeListPODObject) {
        for (let j = 0; j < routeListPODObject._routeList.length; j++) {
            let newRoute = new Route();
            console.log(routeListPODObject._routeList.length)
            newRoute.initialiseFromRoutePDO(routeListPODObject._routeList[j]);
            this._routeList[j] = newRoute;
        }
    }
}


