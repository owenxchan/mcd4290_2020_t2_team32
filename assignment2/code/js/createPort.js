const APIKey = "a8d0780f37824e2780468475a7370763";
const APIurl = "https://api.opencagedata.com/geocode/v1/json"

// Mapbox
mapboxgl.accessToken = 'pk.eyJ1IjoidG5ndTAxMTIiLCJhIjoiY2tjd3Jud21lMDU2NjJ0bG0xeWx0NDcwMSJ9.RFxXb37wxqSmRPqFD1ewvg';
let latitude = null
let longitude = null

//Funtion to create a new Port
function createPort() {

    if (typeof (Storage) !== 'undefined') {

        // create a public dummy object to hold the information that is read out.
        let newPortList = new PortList()
        let portPDOList = JSON.parse(localStorage.getItem(portListKey))
        if (portPDOList !== null) {
            newPortList.initialiseFromPortListPDO(portPDOList)
        }

        // Get Element from HTML
        let namePortRef = document.getElementById("namePort")
        let countryPortRef = document.getElementById("countryPort")
        let typePortRef = document.getElementById("typePort")
        let sizePortRef = document.getElementById("sizePort")
        let locPrecisionPortRef = document.getElementById("locPrecisionPort")

        let namePortValue = namePortRef.value
        let countryPortValue = countryPortRef.value
        let typePortValue = typePortRef.value
        let sizePortValue = sizePortRef.value
        let locPrecisionPortValue = locPrecisionPortRef.value

        if (namePortValue !== "" && countryPortValue !== "" && typePortValue !== "" && sizePortValue !== "" && locPrecisionPortValue !== "") {

            // Save data to the class Port to save in the LocalStorage
            let newPort = new Port(namePortValue, countryPortValue, typePortValue, sizePortValue, locPrecisionPortValue, latitude, longitude)
            newPortList.addToList(newPort)
            localStorage.setItem(portListKey, JSON.stringify(newPortList))
            window.location.href = "viewPorts.html"
        }
        else {
            alert("Please enter all required information!")
        }

    }
    else {
        alert("Local storage is unavailable")
    }
}

// Open Cage get Latitude and Longitude
function opencageResponse(data) {
    latitude = data.results[0].geometry.lat;
    longitude = data.results[0].geometry.lng;
    mapForCreatePort()
}

// Open Cage display information
function opencageRequest() {
    let namePortRef = document.getElementById("namePort")
    let countryPortRef = document.getElementById("countryPort")
    let namePortValue = namePortRef.value
    let countryPortValue = countryPortRef.value

    let request_url = APIurl
        + '?'
        + 'key=' + APIKey
        + '&q=' + encodeURIComponent(namePortValue + ',' + countryPortValue)
        + '&callback=opencageResponse'

    let script = document.createElement('script');
    script.src = request_url;
    document.body.appendChild(script);

    if (namePortValue !== "" && countryPortValue !== "") {
        let button = document.querySelector('button')
        button.disabled = false
    }
}

//Creating a map display to location from open cage
function mapForCreatePort() {

    if (longitude !== null && latitude !== null) {
        // creating map
        let map = new mapboxgl.Map
            ({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v11',
                zoom: 15
            });

        let portLocation = [longitude, latitude]
        // creating a pop in the map
        let Popup = new mapboxgl.Popup({ offset: 15 })
            .setText("New Port")
            .addTo(map)
        // creating a marker in the map
        let Marker = new mapboxgl.Marker({ color: "red" })
            .setLngLat(portLocation)
            .addTo(map)
            .setPopup(Popup);

        map.panTo(portLocation)

    }
}